<?php
   // Include config.php file
   include_once('Department_Config.php');

   $dbObj = new Database();

   // Insert Record   
   if (isset($_POST['action']) && $_POST['action'] == "insert") {

      $code = $_POST['txtCode'];
      $description = $_POST['txtDescription'];
      $createdBy = 'John';
      $createdDate = (new DateTime('now'))->format('Y-m-d H:i:s');
      $dbObj->insertRecord($code, $description, $createdBy, $createdDate);
   }

   // View record
   if (isset($_POST['action']) && $_POST['action'] == "view") {
      $output = "";
      $tCount = 0;
      $departmentList = $dbObj->displayRecord();

      if ($dbObj->totalRowCount() > 0) {
         $output .="<table class='table table-hover'>
                 <thead class='bg-primary text-light'>
                   <tr>
                     <th>No</th>
                     <th>Code</th>
                     <th>Description</th>
                     <th>CreatedBy</th>
                     <th>CreatedDate</th>
                     <th>Action</th>
                   </tr>
                 </thead>
                 <tbody>";
         foreach ($departmentList as $department) {
                    $tCount+=1;
         $output.="<tr>
                     <td>".$tCount."</td>
                     <td>".$department['Code']."</td>
                     <td>".$department['Description']."</td>
                     <td>".$department['CreatedBy']."</td>
                     <td>".date('d-M-Y', strtotime($department['CreatedDate']))."</td>
                     <td>
                       <a href='#editModal' style='color:green' data-toggle='modal' 
                       class='editBtn' id='".$department['DepartmentID']."'><i class='fa fa-pencil'></i></a>&nbsp; 
                       <a href='' style='color:red' class='deleteBtn' id='".$department['DepartmentID']."'>
                       <i class='fa fa-trash' ></i></a>
                     </td>
                 </tr>";
            }
         $output .= "</tbody>
            </table>";
            echo $output;   
      }else{
         echo '<h3 class="text-center mt-5">No records found</h3>';
      }
   }

   // Edit Record  --> Binding Record 
   if (isset($_POST['editId'])) {
      $editId = $_POST['editId'];
      $row = $dbObj->getRecordById($editId);
      echo json_encode($row);
   }

      // Update Record
      if (isset($_POST['action']) && $_POST['action'] == "update") {
        $id = $_POST['id'];
        $code = $_POST['txtCode'];
        $description = $_POST['txtDescription'];
        $lastModifiedBy = 'Paul';
        $lastModifiedDate = (new DateTime('now'))->format('Y-m-d H:i:s');

        $dbObj->updateRecord($id, $code, $description, $lastModifiedBy, $lastModifiedDate);
     }
     // Delete Record   
     if (isset($_POST['deleteBtn'])) {
        $deleteBtn = $_POST['deleteBtn'];
        $dbObj->deleteRecord($deleteBtn);
     }

?>