<?php
   // Include config.php file
   include_once('AcademicYear_Config.php');

   $dbObj = new Database();

   // Insert Record   
   if (isset($_POST['action']) && $_POST['action'] == "insert") {

      $academicYear = $_POST['txtAcademicYear'];
      $startDate = $_POST['dtpStartDate'];
      $endDate = $_POST['dtpEndDate'];
      $finalClosureDate = $_POST['dtpFinalClosureDate'];
      $closureDate = $_POST['dtpClosureDate']; 

      $createdBy = 'John';
      $createdDate = (new DateTime('now'))->format('Y-m-d H:i:s');

      $dbObj->insertRecord($academicYear, $startDate, $endDate , $finalClosureDate, $closureDate, $createdBy, $createdDate);
   }

   // View record
   if (isset($_POST['action']) && $_POST['action'] == "view") {
      $output = "";
      $tCount = 0;
      $academicYearList = $dbObj->displayRecord();

      if ($dbObj->totalRowCount() > 0) {
         $output .="<table class='table table-hover'>
                 <thead class='bg-primary text-light'>
                   <tr>
                     <th>No</th>
                     <th>AcademicYear</th>
                     <th>StartDate</th>
                     <th>EndDate</th>
                     <th>FinalClosureDate</th>
                     <th>ClosureDate</th>
                     <th>CreatedBy</th>
                     <th>CreatedDate</th>
                     <th>Action</th>
                   </tr>
                 </thead>
                 <tbody>";
         foreach ($academicYearList as $academicYear) {
                    $tCount+=1;
         $output.="<tr>
                     <td>".$tCount."</td>
                     <td>".$academicYear['AcademicYear']."</td>
                     <td>".date('d-M-Y', strtotime($academicYear['StartDate']))."</td>
                     <td>".date('d-M-Y', strtotime($academicYear['EndDate']))."</td>
                     <td>".date('d-M-Y', strtotime($academicYear['FinalClosureDate']))."</td>
                     <td>".date('d-M-Y', strtotime($academicYear['ClosureDate']))."</td>
                     <td>".$academicYear['CreatedBy']."</td>
                     <td>".date('d-M-Y', strtotime($academicYear['CreatedDate']))."</td>
                     <td>
                       <a href='#editModal' style='color:green' data-toggle='modal' 
                       class='editBtn' id='".$academicYear['AcademicYearID']."'><i class='fa fa-pencil'></i></a>&nbsp; 
                       <a href='' style='color:red' class='deleteBtn' id='".$academicYear['AcademicYearID']."'>
                       <i class='fa fa-trash' ></i></a>
                     </td>
                 </tr>";
            }
         $output .= "</tbody>
            </table>";
            echo $output;   
      }else{
         echo '<h3 class="text-center mt-5">No records found</h3>';
      }
   }

   // Edit Record  --> Binding Record 
   if (isset($_POST['editId'])) {
      $editId = $_POST['editId'];
      $row = $dbObj->getRecordById($editId);
      echo json_encode($row);
   }

    // Update Record
    if (isset($_POST['action']) && $_POST['action'] == "update") {
        $id = $_POST['id'];
        $academicYear = $_POST['txtAcademicYear'];
        $startDate = $_POST['dtpStartDate'];
        $endDate = $_POST['dtpEndDate'];
        $finalClosureDate = $_POST['dtpFinalClosureDate'];
        $closureDate = $_POST['dtpClosureDate'];

        $lastModifiedBy = 'Paul';
        $lastModifiedDate = (new DateTime('now'))->format('Y-m-d H:i:s');

        $dbObj->updateRecord($id, $academicYear, $startDate, $endDate, $finalClosureDate, $closureDate ,$lastModifiedBy, $lastModifiedDate);
    }
     // Delete Record   
     if (isset($_POST['deleteBtn'])) {
        $deleteBtn = $_POST['deleteBtn'];
        $dbObj->deleteRecord($deleteBtn);
     }

?>