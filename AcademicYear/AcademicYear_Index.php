<!DOCTYPE html>
<html lang="en">

<head>
    <title> Idea </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.22/datatables.min.css" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
</head>

<body>

    <div class="card text-center">
        <h3> AcademicYear </h3>
    </div><br><br>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <button type="button" class="btn btn-primary m-1 float-right" data-toggle="modal" data-target="#addModal">
                    <i class="fa fa-plus"></i> Add New AcademicYear
                </button>
            </div>
        </div><br>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="table-responsive" id="tblAcademicYear">
                    <h3 class="text-center text-success">Loading...</h3>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Record  Modal -->
    <div class="modal fade" id="addModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Add New AcademicYear</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form id="formAcademicYear">
                        <div class="form-group">
                            <label for="txtAcademicYear">AcademicYear: </label>
                            <input type="text" class="form-control" name="txtAcademicYear" placeholder="Enter AcademicYear" required="">
                        </div>
                        <div class="form-group">
                            <label for="dtpStartDate">StartDate: </label>
                            <input type="date" class="form-control" name="dtpStartDate" required="">
                        </div>
                        <div class="form-group">
                            <label for="dtpEndDate">EndDate: </label>
                            <input type="date" class="form-control" name="dtpEndDate"  required="">
                        </div>
                        <div class="form-group">
                            <label for="dtpFinalClosureDate">FinalClosureDate: </label>
                            <input type="date" class="form-control" name="dtpFinalClosureDate" required="">
                        </div>
                        <div class="form-group">
                            <label for="dtpClosureDate">ClosureDate: </label>
                            <input type="date" class="form-control" name="dtpClosureDate" required="">
                        </div>
                        <hr>
                        <div class="form-group float-right">
                            <button type="submit" class="btn btn-success" id="btnSave">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Record  Modal -->
    <div class="modal fade" id="editModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Edit AcademicYear</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form id="EditformAcademicYear">
                        <input type="hidden" name="id" id="edit-form-id">
                        <div class="form-group">
                            <label for="txtAcademicYear">AcademicYear: </label>
                            <input type="text" class="form-control" name="txtAcademicYear" id="txtAcademicYear" placeholder="Enter AcademicYear" required="">
                        </div>
                        <div class="form-group">
                            <label for="dtpStartDate">StartDate: </label>
                            <input type="date" class="form-control" name="dtpStartDate" id="dtpStartDate" placeholder="Enter StartDate" required="">
                        </div>
                        <div class="form-group">
                            <label for="dtpEndDate">EndDate: </label>
                            <input type="date" class="form-control" name="dtpEndDate" id="dtpEndDate" placeholder="Enter EndDate" required="">
                        </div>
                        <div class="form-group">
                            <label for="dtpFinalClosureDate">FinalClosureDate: </label>
                            <input type="date" class="form-control" name="dtpFinalClosureDate" id="dtpFinalClosureDate" placeholder="Enter FinalClosureDate" required="">
                        </div>
                        <div class="form-group">
                            <label for="dtpClosureDate">ClosureDate: </label>
                            <input type="date" class="form-control" name="dtpClosureDate" id="dtpClosureDate" placeholder="Enter ClosureDate" required="">
                        </div>
                        <hr>
                        <div class="form-group float-right">
                            <button type="submit" class="btn btn-primary" id="btnUpdate">Update</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.22/datatables.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            
            showAllAcademicYear();
            //View Record
            function showAllAcademicYear() {
                $.ajax({
                    url: "AcademicYear_Action.php",
                    type: "POST",
                    data: {
                        action: "view"
                    },
                    success: function(response) {
                        $("#tblAcademicYear").html(response);
                        $("table").DataTable({
                            order: [0, 'DESC']
                        });
                    }
                });
            }

            //Insert Record
            $("#btnSave").click(function(e) {
                if ($("#formAcademicYear")[0].checkValidity()) {
                    e.preventDefault();

                    $.ajax({
                        url: "AcademicYear_Action.php",
                        type: "POST",
                        data: $("#formAcademicYear").serialize() + "&action=insert",
                        success: function(response) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Saved successfully!',
                            });
                            $("#addModal").modal('hide');
                            $("#formAcademicYear")[0].reset();
                            showAllAcademicYear();
                        }
                    });
                }
            });

            //Edit Record --> Binding Record
            $("body").on("click", ".editBtn", function(e) {
                e.preventDefault();
                var editId = $(this).attr('id');
                $.ajax({
                    url: "AcademicYear_Action.php",
                    type: "POST",
                    data: {
                        editId: editId
                    },
                    success: function(response) {
                        var data = JSON.parse(response);
                        $("#edit-form-id").val(data.AcademicYearID);
                        $("#txtAcademicYear").val(data.AcademicYear);
                        $("#dtpStartDate").val(data.StartDate);
                        $("#dtpEndDate").val(data.EndDate);
                        $("#dtpFinalClosureDate").val(data.FinalClosureDate);
                        $("#dtpClosureDate").val(data.ClosureDate);
                    }
                });
            });

            //Update Record
            $("#btnUpdate").click(function(e) {
                if ($("#EditformAcademicYear")[0].checkValidity()) {
                    e.preventDefault();
                    $.ajax({
                        url: "AcademicYear_Action.php",
                        type: "POST",
                        data: $("#EditformAcademicYear").serialize() + "&action=update",
                        success: function(response) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Updated successfully!',
                            });
                            $("#editModal").modal('hide');
                            $("#EditformAcademicYear")[0].reset();
                            showAllAcademicYear();
                        }
                    });
                }
            });

            //Delete Record
            $("body").on("click", ".deleteBtn", function(e) {
                e.preventDefault();
                var tr = $(this).closest('tr');
                var deleteBtn = $(this).attr('id');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: "AcademicYear_Action.php",
                            type: "POST",
                            data: {
                                deleteBtn: deleteBtn
                            },
                            success: function(response) {
                                tr.css('background-color', '#ff6565');
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Delete successfully!',
                                });
                                showAllAcademicYear();
                            }
                        });
                    }
                })
            });

        });
    </script>
</body>

</html>