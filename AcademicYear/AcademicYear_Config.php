<?php
   
   class Database 
   {
      private $servername = "localhost";
      private $username   = "root";
      private $password   = "";
      private $dbname = "idea";
      public $con;
      public $academicyearTable = "academicyear";

      public function __construct()
      {
         try {
            $this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);   
         } catch (Exception $e) {
            echo $e->getMessage();
         }
      }

      // Insert Record
      public function insertRecord($academicYear, $startDate, $endDate, $finalClosureDate, $closureDate, $createdBy, $createdDate)
      {
         $sql = "INSERT INTO $this->academicyearTable (AcademicYear, StartDate, EndDate ,FinalClosureDate, ClosureDate, CreatedBy, CreatedDate) 
         VALUES('$academicYear','$startDate', '$endDate','$finalClosureDate','$closureDate','$createdBy','$createdDate')";
         $query = $this->con->query($sql);
         if ($query) {
            return true;
         }else{
            return false;
         }
      }

      // Update Record
      public function updateRecord($id, $academicYear, $startDate, $endDate, $finalClosureDate, $closureDate, $lastModifiedBy, $lastModifiedDate)
      {
         $sql = "UPDATE $this->academicyearTable SET AcademicYear = '$academicYear', StartDate = '$startDate', 
         EndDate = '$endDate', FinalClosureDate = '$finalClosureDate',  ClosureDate = '$closureDate',
         LastModifiedBy = '$lastModifiedBy', LastModifiedDate = '$lastModifiedDate' 
         WHERE AcademicYearID = '$id'";
         $query = $this->con->query($sql);
         if ($query) {
            return true;
         }else{
            return false;
         }
      }
      // Delete Record
      public function deleteRecord($id)
      {
         $sql = "DELETE FROM $this->academicyearTable WHERE AcademicYearID = '$id'";
         $query = $this->con->query($sql);
         if ($query) {
            return true;
         }else{
            return false;
         }
      }


      // Fetch records 
      public function displayRecord()
      {
         $sql = "SELECT * FROM $this->academicyearTable";
         $query = $this->con->query($sql);
         $data = array();
         if ($query->num_rows > 0) {
            while ($row = $query->fetch_assoc()) {
               $data[] = $row;
            }
            return $data;
         }else{
            return false;
         }
      }

      // Fetch single Record for edit
      public function getRecordById($id)
      {
         $query = "SELECT * FROM $this->academicyearTable WHERE AcademicYearID = '$id'";
         $result = $this->con->query($query);
         if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            return $row;
         }else{
            return false;
         }
      }


      public function totalRowCount(){
         $sql = "SELECT * FROM $this->academicyearTable";
         $query = $this->con->query($sql);
         $rowCount = $query->num_rows;
         return $rowCount;
      }

   }
?>