<?php
   
   class Database 
   {
      private $servername = "localhost";
      private $username   = "root";
      private $password   = "";
      private $dbname = "idea";
      public $con;
      public $categoryTable = "category";

      public function __construct()
      {
         try {
            $this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);   
         } catch (Exception $e) {
            echo $e->getMessage();
         }
      }

      // Insert Category Record
      public function insertRecord($code, $description, $createdBy, $createdDate)
      {
         $sql = "INSERT INTO $this->categoryTable (Code, Description, CreatedBy, CreatedDate) VALUES('$code','$description','$createdBy','$createdDate')";
         $query = $this->con->query($sql);
         if ($query) {
            return true;
         }else{
            return false;
         }
      }

      // Update Category Record
      public function updateRecord($id, $code, $description, $lastModifiedBy, $lastModifiedDate)
      {
         $sql = "UPDATE $this->categoryTable SET Code = '$code', Description = '$description', LastModifiedBy = '$lastModifiedBy', LastModifiedDate = '$lastModifiedDate' 
         WHERE CategoryID = '$id'";
         $query = $this->con->query($sql);
         if ($query) {
            return true;
         }else{
            return false;
         }
      }
      // Delete Category Record
      public function deleteRecord($id)
      {
         $sql = "DELETE FROM $this->categoryTable WHERE CategoryID = '$id'";
         $query = $this->con->query($sql);
         if ($query) {
            return true;
         }else{
            return false;
         }
      }


      // Fetch Category records 
      public function displayRecord()
      {
         $sql = "SELECT * FROM $this->categoryTable";
         $query = $this->con->query($sql);
         $data = array();
         if ($query->num_rows > 0) {
            while ($row = $query->fetch_assoc()) {
               $data[] = $row;
            }
            return $data;
         }else{
            return false;
         }
      }

      // Fetch single Category Record for edit
      public function getRecordById($id)
      {
         $query = "SELECT * FROM $this->categoryTable WHERE CategoryID = '$id'";
         $result = $this->con->query($query);
         if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            return $row;
         }else{
            return false;
         }
      }


      public function totalRowCount(){
         $sql = "SELECT * FROM $this->categoryTable";
         $query = $this->con->query($sql);
         $rowCount = $query->num_rows;
         return $rowCount;
      }

   }
?>