<!DOCTYPE html>
<html lang="en">

<head>
    <title> Idea </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.22/datatables.min.css" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
</head>

<body>

    <div class="card text-center">
        <h3> Category </h3>
    </div><br><br>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <button type="button" class="btn btn-primary m-1 float-right" data-toggle="modal" data-target="#addModal">
                    <i class="fa fa-plus"></i> Add New Category
                </button>
            </div>
        </div><br>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="table-responsive" id="tableData">
                    <h3 class="text-center text-success">Loading...</h3>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Record  Modal -->
    <div class="modal fade" id="addModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Add New Category</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form id="formData">
                        <div class="form-group">
                            <label for="txtCategoryCode">Code: </label>
                            <input type="text" class="form-control" name="txtCategoryCode" placeholder="Enter Code" required="">
                        </div>
                        <div class="form-group">
                            <label for="txtDescription">Description: </label>
                            <!--<input type="text" class="form-control" name="txtDescription" placeholder="Enter Description" required=""> -->
                            <textarea name="txtDescription" class="form-control" cols="40" rows="5" placeholder="Enter Description" required=""></textarea>
                        </div>
                        <hr>
                        <div class="form-group float-right">
                            <button type="submit" class="btn btn-success" id="submit">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Record  Modal -->
    <div class="modal fade" id="editModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Edit Category</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form id="EditformData">
                        <input type="hidden" name="id" id="edit-form-id">
                        <div class="form-group">
                            <label for="txtCategoryCode">Code: </label>
                            <input type="text" class="form-control" name="txtCategoryCode" id="txtCategoryCode" placeholder="Enter Code" required="">
                        </div>
                        <div class="form-group">
                            <label for="txtDescription">Description: </label>
                            <!-- <input type="text" class="form-control" name="txtDescription" id="txtDescription" placeholder="Enter Description" required=""> -->
                            <textarea name="txtDescription" id="txtDescription" class="form-control" cols="40" rows="5" placeholder="Enter Description" required=""></textarea>
                        </div>
                        <hr>
                        <div class="form-group float-right">
                            <button type="submit" class="btn btn-primary" id="update">Update</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.22/datatables.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            showAllCategory();
            //View Record
            function showAllCategory() {
                $.ajax({
                    url: "Category_Action.php",
                    type: "POST",
                    data: {
                        action: "view"
                    },
                    success: function(response) {
                        $("#tableData").html(response);
                        $("table").DataTable({
                            order: [0, 'DESC']
                        });
                    }
                });
            }

            //insert Record
            $("#submit").click(function(e) {
                if ($("#formData")[0].checkValidity()) {
                    e.preventDefault();
                    $.ajax({
                        url: "Category_Action.php",
                        type: "POST",
                        data: $("#formData").serialize() + "&action=insert",
                        success: function(response) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Saved successfully',
                            });
                            $("#addModal").modal('hide');
                            $("#formData")[0].reset();
                            showAllCategory();
                        }
                    });
                }
            });

            //Edit Record
            $("body").on("click", ".editBtn", function(e) {
                e.preventDefault();
                var editId = $(this).attr('id');
                $.ajax({
                    url: "Category_Action.php",
                    type: "POST",
                    data: {
                        editId: editId
                    },
                    success: function(response) {
                        var data = JSON.parse(response);
                        $("#edit-form-id").val(data.CategoryID);
                        $("#txtCategoryCode").val(data.Code);
                        $("#txtDescription").val(data.Description);
                    }
                });
            });


            //update Record
            $("#update").click(function(e) {
                if ($("#EditformData")[0].checkValidity()) {
                    e.preventDefault();
                    $.ajax({
                        url: "Category_Action.php",
                        type: "POST",
                        data: $("#EditformData").serialize() + "&action=update",
                        success: function(response) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Updated successfully',
                            });
                            $("#editModal").modal('hide');
                            $("#EditformData")[0].reset();
                            showAllCategory();
                        }
                    });
                }
            });

            //Delete Record
            $("body").on("click", ".deleteBtn", function(e) {
                e.preventDefault();
                var tr = $(this).closest('tr');
                var deleteBtn = $(this).attr('id');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: "Category_Action.php",
                            type: "POST",
                            data: {
                                deleteBtn: deleteBtn
                            },
                            success: function(response) {
                                tr.css('background-color', '#ff6565');
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Delete successfully!',
                                });
                                showAllCategory();
                            }
                        });
                    }
                })


                /* if (confirm('Are you sure want to delete this Record?')) {
                   $.ajax({
                     url : "Category_Action.php",
                     type : "POST",
                     data : {deleteBtn:deleteBtn},
                     success:function(response){
                       tr.css('background-color','#ff6565');
                       Swal.fire({
                         icon: 'success',
                         title: 'Category delete successfully!',
                       });
                       showAllCategory();
                     }
                   });
                 
                 } */


            });

        });
    </script>
</body>

</html>