<?php
   // Include config.php file
   include_once('Category_Config.php');

   $dbObj = new Database();

   // Insert Record   
   if (isset($_POST['action']) && $_POST['action'] == "insert") {

      $code = $_POST['txtCategoryCode'];
      $description = $_POST['txtDescription'];
      $createdBy = 'John';
      $createdDate = (new DateTime('now'))->format('Y-m-d H:i:s');
      $dbObj->insertRecord($code, $description, $createdBy, $createdDate);
   }

   // View record
   if (isset($_POST['action']) && $_POST['action'] == "view") {
      $output = "";
      $tCount = 0;
      $categoryList = $dbObj->displayRecord();

      if ($dbObj->totalRowCount() > 0) {
         $output .="<table class='table table-hover'>
                 <thead class='bg-primary text-light'>
                   <tr>
                     <th>No</th>
                     <th>Code</th>
                     <th>Description</th>
                     <th>CreatedBy</th>
                     <th>CreatedDate</th>
                     <th>Action</th>
                   </tr>
                 </thead>
                 <tbody>";
         foreach ($categoryList as $category) {
                    $tCount+=1;
         $output.="<tr>
                     <td>".$tCount."</td>
                     <td>".$category['Code']."</td>
                     <td>".$category['Description']."</td>
                     <td>".$category['CreatedBy']."</td>
                     <td>".date('d-M-Y', strtotime($category['CreatedDate']))."</td>
                     <td>
                       <a href='#editModal' style='color:green' data-toggle='modal' 
                       class='editBtn' id='".$category['CategoryID']."'><i class='fa fa-pencil'></i></a>&nbsp;
                       <a href='' style='color:red' class='deleteBtn' id='".$category['CategoryID']."'>
                       <i class='fa fa-trash' ></i></a>
                     </td>
                 </tr>";
            }
         $output .= "</tbody>
            </table>";
            echo $output;   
      }else{
         echo '<h3 class="text-center mt-5">No records found</h3>';
      }
   }

   // Edit Record   
   if (isset($_POST['editId'])) {
      $editId = $_POST['editId'];
      $row = $dbObj->getRecordById($editId);
      echo json_encode($row);
   }

      // Update Record
      if (isset($_POST['action']) && $_POST['action'] == "update") {
        $id = $_POST['id'];
        $code = $_POST['txtCategoryCode'];
        $description = $_POST['txtDescription'];
        $lastModifiedBy = 'Paul';
        $lastModifiedDate = (new DateTime('now'))->format('Y-m-d H:i:s');

        $dbObj->updateRecord($id, $code, $description, $lastModifiedBy, $lastModifiedDate);
     }
     // Delete Record   
     if (isset($_POST['deleteBtn'])) {
        $deleteBtn = $_POST['deleteBtn'];
        $dbObj->deleteRecord($deleteBtn);
     }

?>